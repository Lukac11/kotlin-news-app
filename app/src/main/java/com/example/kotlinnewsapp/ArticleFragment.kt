package com.example.kotlinnewsapp

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_article.*
import kotlin.properties.Delegates


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ArticleFragment : Fragment() {

    private lateinit var itemList: ItemList
    private var position: Int = 5
    private lateinit var newsList: List<Article>
    var picasso = Picasso.get()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            itemList = it.getSerializable(ARG_PARAM1) as ItemList
            position = it.getInt(ARG_PARAM2)
        }

        newsList = itemList.getNewsList()
        Log.d(ContentValues.TAG, "NewsListTitle: " + newsList.get(position).title)
        Log.d(TAG, "Position: " + position)

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_article, container, false)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setParameters()
    }

    fun setParameters() {
        tvTitle.text = newsList.get(position).title
        tv_Description.text = newsList.get(position).description
        picasso.load(newsList[position].urlToImage).into(iv_Article)

    }


    companion object {

        @JvmStatic
        fun newInstance(param1: ItemList, param2: Int) =
                ArticleFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(ARG_PARAM1, param1)
                        putInt(ARG_PARAM2, param2)
                    }
                }
    }
}