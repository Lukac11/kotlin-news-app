package com.example.kotlinnewsapp

import java.io.Serializable

class ItemList : Serializable {
    private var newsList = emptyList<Article>()
    fun getNewsList(): List<Article> {
        return newsList
    }

    fun setNewsList(list: List<Article>) {
        newsList = list
    }

}
