package com.example.kotlinnewsapp

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://newsapi.org/v1/"

class MainActivity : AppCompatActivity() {
    lateinit var recAdapter: RecyclerAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    private var newsList = ItemList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getMyData()
        setUpRecycler()
        setUpItemListener()
    }

    private fun getMyData() {
        val retrofitBuilder = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(BASE_URL).build().create(ApiInterface::class.java)
        val retrofitData = retrofitBuilder.getData()
        retrofitData.enqueue(object : Callback<MyData?> {
            override fun onResponse(call: Call<MyData?>, response: Response<MyData?>) {
                val responseBody = response.body()!!
                recAdapter.addData(responseBody.articles)
                newsList.setNewsList(responseBody.articles)
            }

            override fun onFailure(call: Call<MyData?>, t: Throwable) {
                alertUser()
            }
        })
    }

    fun setUpRecycler() {
        recAdapter = RecyclerAdapter(this)
        recView.adapter = recAdapter
        linearLayoutManager = LinearLayoutManager(this)
        recView.layoutManager = linearLayoutManager;
    }

    fun setUpItemListener() {
        recAdapter.setOnItemCLickListener(object : RecyclerAdapter.onItemClickListener {
            override fun onItemClick(position: Int) {
                openScreenSlidePagerActivity(position)
            }
        })
    }

    fun openScreenSlidePagerActivity(position: Int) {
        intent = Intent(
                this@MainActivity,
                ScreenSlidePagerActivity::class.java
        )
        intent.putExtra(ScreenSlidePagerActivity.NEWS_LIST, newsList)
        intent.putExtra(ScreenSlidePagerActivity.POSITION, position)
        Log.d(ContentValues.TAG, "onItemClick: " + intent.getIntExtra("Position", 2))
        startActivity(intent)
    }

    fun alertUser() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.warning))
        builder.setMessage(R.string.warning_message)
        builder.setPositiveButton("Ok") { dialog, which ->
            Toast.makeText(applicationContext,
                    R.string.api_error, Toast.LENGTH_SHORT).show()
        }
        builder.show()
    }
}