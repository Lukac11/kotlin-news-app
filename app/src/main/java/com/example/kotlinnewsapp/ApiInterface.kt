package com.example.kotlinnewsapp

import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {
    @GET("articles?source=bbc-news&sortBy=top&apiKey=ed915c78ec9f43f3ad165198009efb2e")
    fun getData(): Call<MyData>
}