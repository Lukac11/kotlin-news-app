package com.example.kotlinnewsapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class RecyclerAdapter(val context: Context) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {
    private lateinit var mListener: onItemClickListener
    val picasso = Picasso.get()
    var newsList: List<Article>

    init {
        newsList = ArrayList<Article>()
    }

    interface onItemClickListener {
        fun onItemClick(position: Int)
    }

    class ViewHolder(view: View, listener: onItemClickListener) : RecyclerView.ViewHolder(view) {
        var title: TextView
        var image: ImageView

        init {
            view.setOnClickListener {
                listener.onItemClick(adapterPosition)
            }
            title = view.findViewById(R.id.title)
            image = view.findViewById(R.id.icon)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var itemView = LayoutInflater.from(context).inflate(R.layout.row, parent, false)
        return ViewHolder(itemView, mListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = newsList[position].title
        picasso.load(newsList[position].urlToImage).into(holder.image)
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    fun addData(dataList: List<Article>) {
        this.newsList = dataList;
        notifyDataSetChanged()
    }

    fun setOnItemCLickListener(listener: onItemClickListener) {
        mListener = listener
    }


}
