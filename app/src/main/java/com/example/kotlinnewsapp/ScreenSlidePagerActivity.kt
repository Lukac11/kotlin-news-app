package com.example.kotlinnewsapp

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2

class ScreenSlidePagerActivity : FragmentActivity() {

    private lateinit var mPager: ViewPager2
    private var mPosition: Int = 0
    private lateinit var itemList: ItemList
    private var newsList = emptyList<Article>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen_slide_pager)
        mPosition = intent.getIntExtra(POSITION, 2)
        itemList = intent.getSerializableExtra(NEWS_LIST) as ItemList
        newsList = itemList.getNewsList()
        Log.d(TAG, "Position after getting intent" + mPosition)
        Log.d(TAG, "Newslist: " + newsList.get(mPosition).title)

        setUpPager()


    }


    fun setPosition() {
        mPager.currentItem = mPosition
    }

    fun setUpPager() {
        mPager = findViewById(R.id.viewPager)
        val pagerAdapter = ScreenSlidePagerAdapter(this)
        mPager.adapter = pagerAdapter
        setPosition()
    }

    companion object {
        const val POSITION = "Position"
        const val NEWS_LIST = "NewsList"
    }


    private inner class ScreenSlidePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

        override fun getItemCount(): Int {
            return newsList.size
        }

        override fun createFragment(position: Int): Fragment = ArticleFragment.newInstance(itemList, position)

    }

}